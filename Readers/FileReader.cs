﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using RpgMakerReader.Helpers;

namespace RpgMakerReader.Readers
{
    public abstract class FileReader<T>
    {
        protected BinaryReader reader;
        protected string filename;

        protected abstract T Rm2kObject { get; }
        protected abstract string TypeIdentifier { get; }

        public FileReader(string filename)
        {
            this.filename = filename;
        }

        public T Read()
        {
            using (reader = new BinaryReader(File.OpenRead(filename)))
            {
                ReadHeader();
                ReadImplementation();
            }

            return Rm2kObject;
        }

        protected void ReadHeader()
        {
            // Read header
            uint headerLength = Utility.ReadBer(reader);
            string header = Utility.ReadString(reader, headerLength);
            if (header != TypeIdentifier)
            {
                throw new ArgumentException("Invalid file format.");
            }
            Console.WriteLine("Header: " + header);
        }


        protected abstract void ReadImplementation();
    }
}
