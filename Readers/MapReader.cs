﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RpgMakerReader.Types;
using RpgMakerReader.Helpers;

namespace RpgMakerReader.Readers
{
    public class MapReader : FileReader<Map>
    {
        // Map formats
        private enum MapTags
        {
            ChipsetID = 0x1,
            Width = 0x02,
            Height = 0x03,
            LoopType = 0x0B,
            HasPanorama = 0x1F,
            PanoramaName = 0x20,
            HasHorizontalScroll = 0x21,
            HasVerticalScroll = 0x22,
            HasAutoHorizontalScroll = 0x23,
            AutoHorizontalScrollSpeed = 0x24,
            HasAutoVerticalScroll = 0x25,
            AutoVerticalScrollSpeed = 0x26,
            LowerLayer = 0x47,
            UpperLayer = 0x48,
            Events = 0x51,
            SaveTime = 0x5B,
        }
        private Map map;
        protected override Map Rm2kObject
        {
            get { return map; }
        }
        protected override string TypeIdentifier
        {
            get { return "LcfMapUnit"; }
        }

        public MapReader(string filename) : base(filename)
        {
            this.map = new Map();
        }

        protected override void ReadImplementation()
        {
            ReadProperties();
            ReadLowerTiles();
        }

        private void ReadProperties()
        {
            for (; ; )
            {
                byte tagByte = reader.ReadByte();
                if (tagByte == (byte)MapTags.LowerLayer)
                    break;
                if (Enum.IsDefined(typeof(MapTags), (int)tagByte))
                {
                    MapTags tag = (MapTags)tagByte;
                    // Read length
                    uint length = Utility.ReadBer(reader);
                    // Read Value
                    object value;
                    if (length > 1)
                    {
                        value = Utility.ReadString(reader, length);
                        Console.WriteLine(tag.ToString() + ": " + value);
                    }
                    else
                    {
                        value = Utility.ReadBer(reader);
                        Console.WriteLine(tag.ToString() + ": " + value);
                    }
                    var tagString = tag.ToString();
                    // TODO
                    if (tagString == "ChipsetID")
                        tagString = "TilesetID";
                    var property = typeof(Map).GetProperty(tagString);
                    
                    if (property.PropertyType == typeof(Boolean))
                        value = ((UInt32)value) == 1;
                    if (property.PropertyType.IsEnum)
                        value = Enum.ToObject(property.PropertyType, value);
                    if (property.PropertyType != value.GetType())
                    {
                        value = Convert.ChangeType(value, property.PropertyType);
                    }
                    property.SetValue(map, value, null);
                }
            }
        }
        
        private void ReadLowerTiles()
        {
            uint lowerSize = Utility.ReadBer(reader);
            Console.WriteLine("Lower tile bytes: " + lowerSize);
            map.LowerLayer = new uint[lowerSize / 2];
            for (int i = 0; i < map.LowerLayer.Length; ++i)
            {
                ushort value = reader.ReadUInt16();
                if (value >= 0x0FA0 && value < 0x11F8)
                {
                    Console.WriteLine("{0:X}", value);
                    map.LowerLayer[i] = TileConverter.ConvertTile(value);
                }
            }
        }
    }
}
