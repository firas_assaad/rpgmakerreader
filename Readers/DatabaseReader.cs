﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RpgMakerReader.Types;
using RpgMakerReader.Helpers;
using RpgMakerReader.Readers.DatabaseSectionReaders;

namespace RpgMakerReader.Readers
{
    // http://rpgtukuru-iphone.googlecode.com/svn/branches/Qt_rpg2kLib/define/LcfDataBase
    // http://rpg2kdev.sue445.net/?RPG2000%2F%A5%C7%A1%BC%A5%BF%A5%D9%A1%BC%A5%B9
    // https://easy-rpg.org/wiki/development/data-structure-reference/lcfdatabase
    public class DatabaseReader : FileReader<Database>
    {
        private enum DatabaseTags
        {
            Heroes = 0x0B,
            Skills = 0x0C,
            Items = 0x0D,
            Monsters = 0x0E,
            MonsterGroups = 0x0F,
            Terrains = 0x10,
            Attributes = 0x11,
            Conditions = 0x12,
            BattleAnimations = 0x13,
            Tilesets = 0x14,
            Terms = 0x15,
            System = 0x16,
            Switches = 0x17,
            Variables = 0x18,
            CommonEvents = 0x19,
        }

        private Database database = new Database();
        protected override Database Rm2kObject
        {
            get { return database; }
        }
        protected override string TypeIdentifier
        {
            get { return "LcfDataBase"; }
        }

        public DatabaseReader(string filename) : base(filename)
        {
        }

        protected override void ReadImplementation()
        {
            for (;;)
            {
                byte tagByte = reader.ReadByte();
                if (tagByte == (byte)DatabaseTags.Tilesets)
                    break;
                if (Enum.IsDefined(typeof(DatabaseTags), (int)tagByte))
                {
                    DatabaseTags tag = (DatabaseTags)tagByte;
                    // Read length
                    uint length = Utility.ReadBer(reader);
                    Console.WriteLine("Skipping section " + tag.ToString() + " of length " + length + " bytes");
                    reader.ReadBytes((int)length);
                }
            }
            database.tilesets = new TilesetReader(reader).ReadTilesets();
        }
    }
}
