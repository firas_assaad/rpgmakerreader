﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using RpgMakerReader.Helpers;
using RpgMakerReader.Types;
using RpgMakerReader.Writers;

namespace RpgMakerReader.Readers
{
    public class ProjectReader
    {
        private string sourcePath;
        private string destPath;

        public ProjectReader(string sourcePath)
        {
            string directory = Path.GetDirectoryName(sourcePath);
            string converted = Constants.Directories.DestinationPrefix;
            string filename = converted + Path.GetFileName(sourcePath);
            string dest = Path.Combine(directory, filename);
            SetPaths(sourcePath, dest);
        }

        public ProjectReader(string sourcePath, string destPath)
        {
            SetPaths(sourcePath, destPath);
        }

        public void SetPaths(string sourcePath, string destPath)
        {
            if (!Directory.Exists(sourcePath))
                throw new ArgumentException("The directory at " + sourcePath + " does not exist.");
            CreateDestDirectory(destPath);
            this.sourcePath = sourcePath;
            this.destPath = destPath;
        }

        public void CreateDestDirectory(string destPath)
        {
            if (!Directory.Exists(destPath))
                Directory.CreateDirectory(destPath);
            var subDirectories = new string[] { 
                Constants.Directories.DestTilesets,
                Constants.Directories.DestMaps,
                Path.Combine(Constants.Directories.DestTilesets,
                    Constants.Directories.DestImages),
                Path.Combine(Constants.Directories.DestTilesets,
                    Constants.Directories.DestImages,
                    Constants.Directories.DestSpecialTiles),
            };
            foreach (var subDirectory in subDirectories)
            {
                var path = Path.Combine(destPath, subDirectory);
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);
            }
        }

        public void Convert()
        {
            var dbReader = new DatabaseReader(Path.Combine(sourcePath, Constants.Files.Database));
            var database = dbReader.Read();
            foreach (var tileset in database.tilesets)
            {
                var tilesetWriter = new TilesetWriter(tileset, sourcePath, destPath);
                tilesetWriter.WriteTilesetImage();
            }
            var mapFilesPaths = Directory.GetFiles(sourcePath, Constants.Files.MapPattern);
            foreach (var mapFilePath in mapFilesPaths)
            {
                var mapReader = new MapReader(mapFilePath);
                var map = mapReader.Read();
                var filename = Path.GetFileNameWithoutExtension(mapFilePath) +
                    Constants.Files.MapExtension;
                var mapDirectory = Path.Combine(destPath, Constants.Directories.DestMaps);
                Map.Save(map, Path.Combine(mapDirectory, filename));
            }
            
        }
    }
}
