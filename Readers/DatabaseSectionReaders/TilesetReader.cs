﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using RpgMakerReader.Types;
using RpgMakerReader.Helpers;

namespace RpgMakerReader.Readers.DatabaseSectionReaders
{
    internal class TilesetReader
    {
        private enum TilesetTags
        {
            Name = 0x1,
            Filename = 0x2,
            TerrainTypes = 0x3,
            LowerPassageInfo = 0x4,
            UpperPassageInfo = 0x5,
            SeaAnimationType = 0xB,
            SeaAnimationSpeed = 0xC,
        }
        private BinaryReader reader;

        public TilesetReader(BinaryReader reader)
        {
            this.reader = reader;
        }

        public Tileset[] ReadTilesets()
        {
            uint length = Utility.ReadBer(reader);
            uint numberOfTilesets = Utility.ReadBer(reader);
            var tilesets = new Tileset[numberOfTilesets];
            Console.WriteLine("Reading " + numberOfTilesets + " tilesets of size " + length + " bytes");
            for (int i = 0; i < numberOfTilesets; i++)
            {
                tilesets[i] = ReadTileset();
            }
            return tilesets;
        }

        private Tileset ReadTileset()
        {
            var tileset = new Tileset();
            tileset.Identifier = (int)Utility.ReadBer(reader);
            Console.WriteLine("Tileset ID: " + tileset.Identifier);
            for (; ; )
            {
                int tagByte = (int) Utility.ReadBer(reader);
                TilesetTags tag;
                if (Enum.IsDefined(typeof(TilesetTags), tagByte))
                {
                    tag = (TilesetTags)tagByte;
                }
                else
                {
                    break;
                }
                switch(tag)
                {
                    case TilesetTags.Name:
                        tileset.Name = Utility.ReadString(reader);
                        Console.WriteLine("Tileset Name: " + tileset.Name);
                        break;
                    case TilesetTags.Filename:
                        tileset.Filename = Utility.ReadString(reader);
                        Console.WriteLine("Tileset Filename: " + tileset.Filename);
                        break;
                    case TilesetTags.TerrainTypes:
                        tileset.TerrainTypes = ReadTerrainTypes();
                        break;
                    case TilesetTags.LowerPassageInfo:
                        tileset.LowerPassageInfo = ReadPassageInfo();
                        break;
                    case TilesetTags.UpperPassageInfo:
                        tileset.UpperPassageInfo = ReadPassageInfo();
                        break;
                    case TilesetTags.SeaAnimationType:
                        // Length (1)
                        Utility.ReadBer(reader);
                        // Value
                        tileset.SeaAnimationType = (Tileset.SeaAnimationTypes) Utility.ReadBer(reader);
                        Console.WriteLine("Sea Animation Type: " + tileset.SeaAnimationType);
                        break;
                    case TilesetTags.SeaAnimationSpeed:
                        // Length (1)
                        Utility.ReadBer(reader);
                        // Value
                        tileset.SeaAnimationSpeed = (Tileset.SeaAnimationSpeeds)Utility.ReadBer(reader);
                        Console.WriteLine("Sea Animation Speed: " + tileset.SeaAnimationSpeed);
                        break;
                }
            }
            return tileset;
        }

        private uint[] ReadTerrainTypes()
        {
            uint length = Utility.ReadBer(reader);
            uint number = length / 2;
            var terrain = new uint[number];
            Console.WriteLine("Terrain types (" + number + " lower tiles):");
            for (int i = 0; i < number; ++i)
            {
                terrain[i] = (uint) reader.ReadUInt16();
            }
            Console.WriteLine("[" + String.Join(",", terrain) + "]");
            return terrain;
        }

        public uint[] ReadPassageInfo()
        {
            uint length = Utility.ReadBer(reader);
            var passageInfo = new uint[length];
            var type = length == 162 ? " lower tiles):" : " upper tiles):";
            Console.WriteLine("Passage info (" + length + type);
            for (int i = 0; i < length; ++i)
            {
                passageInfo[i] = (uint)reader.ReadByte();
            }
            Console.WriteLine("[" + String.Join(",", passageInfo) + "]");
            return passageInfo;
        }
    }
}
