﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using RpgMakerReader.Readers;

namespace RpgMakerReader
{
    // https://easy-rpg.org/wiki/development/data-structure-reference/lcfmapunit
    // http://rpg2kdev.sue445.net/?RPG2000
    // http://code.google.com/p/rpgck/
    // https://github.com/take-cheeze/librpg2k
    class Program
    {
        static void Main(string[] args)
        {
            var projectReader = new ProjectReader("Test");
            projectReader.Convert();
            Console.ReadLine();

        }
    }
}
