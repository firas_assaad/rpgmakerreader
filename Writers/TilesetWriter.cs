﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RpgMakerReader.Types;
using System.IO;
using System.Drawing;
using RpgMakerReader.Helpers;

namespace RpgMakerReader.Writers
{
    public class TilesetWriter
    {
        private Tileset tileset;
        private Bitmap sourceBitmap;
        private Bitmap destTilesetBitmap;
        private Bitmap[] destAutotileBitmaps;
        private Bitmap[] destAnimatedBitmaps;
        private Graphics graphics;
        private string destTilesetFilename;
        private string destAutotileFilename;
        private string destAnimatedFilename;
        public TilesetWriter(Tileset tileset, string sourcePath, string destPath)
        {
            this.tileset = tileset;
            string filename = tileset.Filename + Constants.Files.ImageFormat;
            // Get source image from source folder or from RTP
            string sourceFilename = Path.Combine(sourcePath, 
                Constants.Directories.SrcTilesets, filename);
            if (!File.Exists(sourceFilename))
            {
                sourceFilename = Path.Combine(Constants.Directories.RTP2000,
                Constants.Directories.SrcTilesets, filename);
            }
            sourceBitmap = new Bitmap(sourceFilename);
            // Create tileset bitmap for normal tiles
            string destTilesetFolder = Path.Combine(destPath,
                Constants.Directories.DestTilesets,
                Constants.Directories.DestImages);
            destTilesetFilename = Path.Combine(destTilesetFolder, filename);
            int tileWidth = Constants.Tiles.Width;
            int tilesPerRow = Constants.Tiles.DestTilesPerRow;
            int destWidth = tilesPerRow * tileWidth;
            int destHeight = (Constants.Tiles.NormalTiles / tilesPerRow) * tileWidth;
            destTilesetBitmap = new Bitmap(destWidth, destHeight);
            
            // Create bitmaps for autotiles
            string destSpecialFolder = Path.Combine(destTilesetFolder,
                Constants.Directories.DestSpecialTiles);
            destAutotileFilename = Path.Combine(destSpecialFolder,
                BaseSpecialFilename(Constants.Files.Autotile));
            destAutotileBitmaps = new Bitmap[Constants.Tiles.AutoTiles];
            for (int i = 0; i < destAutotileBitmaps.Length; i++)
            {
                int width;
                if (i < Constants.Tiles.AnimatedAutoTiles)
                    width = Constants.Tiles.AnimatedAutoTilesWidth;
                else
                    width = Constants.Tiles.AutoTilesWidth;
                int height = Constants.Tiles.AutoTilesHeight;
                destAutotileBitmaps[i] = new Bitmap(width, height);
            }

            // Create bitmaps for animated tiles
            destAnimatedFilename = Path.Combine(destSpecialFolder,
                BaseSpecialFilename(Constants.Files.Animated));
            destAnimatedBitmaps = new Bitmap[Constants.Tiles.AnimatedTiles];
            for (int i = 0; i < destAnimatedBitmaps.Length; i++)
            {
                int width = Constants.Tiles.AnimatedWidth;
                int height = Constants.Tiles.Width;
                destAnimatedBitmaps[i] = new Bitmap(width, height);
            }
        }

        public void WriteTilesetImage()
        {
            
            // Draw shallow sea autotile
            graphics = Graphics.FromImage(destAutotileBitmaps[0]);
            DrawShallowWater(0, 0);
            DrawShallowWater(0, 1);
            DrawShallowWater(0, 2);
            string filename = destAutotileFilename + "0" + Constants.Files.ImageFormat;
            destAutotileBitmaps[0].Save(filename);
            // Draw shallow sea (snow) autotile
            graphics = Graphics.FromImage(destAutotileBitmaps[1]);
            DrawShallowWater(1, 0);
            DrawShallowWater(1, 1);
            DrawShallowWater(1, 2);
            filename = destAutotileFilename + "1" + Constants.Files.ImageFormat;
            destAutotileBitmaps[1].Save(filename);

            // Draw deep sea autotile
            graphics = Graphics.FromImage(destAutotileBitmaps[2]);
            DrawDeepWater(0);
            DrawDeepWater(1);
            DrawDeepWater(2);
            filename = destAutotileFilename + "2" + Constants.Files.ImageFormat;
            destAutotileBitmaps[2].Save(filename);

            graphics = Graphics.FromImage(destTilesetBitmap);
            destTilesetBitmap.Save(destTilesetFilename);
        }

        // Draw shallow water auto tile
        // type: 
        //      0 - first type (grass),
        //      1 - second type (snow),
        private void DrawShallowWater(int type, int frame)
        {
            int tileWidth = Constants.Tiles.Width;
            int partWidth = tileWidth / 2;
            int typeOffset = type == 0 || type == 2 ? 0 : 48;
            int frameOffset = frame * tileWidth;
            int offset = typeOffset + frameOffset;
            int destOffset = frame * Constants.Tiles.AutoTilesWidth;
            // First Frame
            // First Row
            DrawTile(destOffset, 0, offset, 0);
            DrawTile(destOffset + tileWidth, 0, 16, 128);
            DrawTile(destOffset + tileWidth * 2, 0, offset, 48);
            // Second Row
            // Left Corner
            DrawTile(destOffset, tileWidth,
                offset, 0, partWidth, partWidth);
            DrawTile(destOffset + partWidth, tileWidth,
                8 + offset, 32, partWidth, partWidth);
            DrawTile(destOffset, tileWidth + partWidth,
                offset, 24, partWidth, partWidth);
            DrawTile(destOffset + partWidth, tileWidth + partWidth,
                frameOffset + 8, 72, partWidth, partWidth);
            // Middle
            DrawTile(destOffset + tileWidth, tileWidth,
                offset, 32, tileWidth, partWidth);
            DrawTile(destOffset + tileWidth, tileWidth + partWidth,
                frameOffset, 72, tileWidth, partWidth);
            // Right Corner
            DrawTile(destOffset + tileWidth * 2, tileWidth,
                offset, 32, partWidth, partWidth);
            DrawTile(destOffset + tileWidth * 2 + partWidth, tileWidth,
                8 + offset, 0, partWidth, partWidth);
            DrawTile(destOffset + tileWidth * 2, tileWidth + partWidth,
                frameOffset, 72, partWidth, partWidth);
            DrawTile(destOffset + tileWidth * 2 + partWidth, tileWidth + partWidth,
                8 + offset, 24, partWidth, partWidth);
            // Third Row
            // Left
            DrawTile(destOffset, tileWidth * 2,
                offset, 16, partWidth, tileWidth);
            DrawTile(destOffset + partWidth, tileWidth * 2,
                frameOffset + 8, 64, partWidth, tileWidth);
            // Middle
            DrawTile(destOffset + tileWidth, tileWidth * 2, frameOffset, 64);
            // Right
            DrawTile(destOffset + tileWidth * 2, tileWidth * 2,
                frameOffset, 64, partWidth, tileWidth);
            DrawTile(destOffset + tileWidth * 2 + partWidth, tileWidth * 2,
                offset + 8, 16, partWidth, tileWidth);
            // Fourth Row
            // Left Corner
            DrawTile(destOffset, tileWidth * 3,
                offset, 16, partWidth, partWidth);
            DrawTile(destOffset + partWidth, tileWidth * 3,
                frameOffset + 8, 64, partWidth, partWidth);
            DrawTile(destOffset, tileWidth * 3 + partWidth,
                offset, 8, partWidth, partWidth);
            DrawTile(destOffset + partWidth, tileWidth * 3 + partWidth,
                offset + 8, 40, partWidth, partWidth);
            // Middle
            DrawTile(destOffset + tileWidth, tileWidth * 3,
                frameOffset, 64, tileWidth, partWidth);
            DrawTile(destOffset + tileWidth, tileWidth * 3 + partWidth,
                offset, 40, tileWidth, partWidth);
            // Right Corner
            DrawTile(destOffset + tileWidth * 2, tileWidth * 3,
                frameOffset, 64, partWidth, partWidth);
            DrawTile(destOffset + tileWidth * 2 + partWidth, tileWidth * 3,
                offset + 8, 16, partWidth, partWidth);
            DrawTile(destOffset + tileWidth * 2, tileWidth * 3 + partWidth,
                offset, 40, partWidth, partWidth);
            DrawTile(destOffset + tileWidth * 2 + partWidth, tileWidth * 3 + partWidth,
                offset + 8, 8, partWidth, partWidth);
        }

        // Draw deep water auto tile
        private void DrawDeepWater(int frame)
        {
            // Draw shallow water as base
            DrawShallowWater(0, frame);

            int tileWidth = Constants.Tiles.Width;
            int partWidth = tileWidth / 2;
            int offset = frame * tileWidth;
            int destOffset = frame * Constants.Tiles.AutoTilesWidth;
            // First Frame
            // First Row
            DrawTile(destOffset, 0, offset, 96);
            DrawTile(destOffset + tileWidth, 0, 0, 0);
            DrawTile(destOffset + tileWidth * 2, 0, offset, 96);
            // Second Row
            // Left Corner
            DrawTile(destOffset + partWidth, tileWidth + partWidth,
                offset, 96, partWidth, partWidth);
            // Middle
            DrawTile(destOffset + tileWidth, tileWidth + partWidth,
                offset, 120, tileWidth, partWidth);
            // Right Corner
            DrawTile(destOffset + tileWidth * 2, tileWidth + partWidth,
                offset + 8, 96, partWidth, partWidth);
            // Third Row
            // Left
            DrawTile(destOffset + partWidth, tileWidth * 2,
                offset + 8, 112, partWidth, tileWidth);
            // Middle
            DrawTile(destOffset + tileWidth, tileWidth * 2, offset, 112);
            // Right
            DrawTile(destOffset + tileWidth * 2, tileWidth * 2,
                offset, 112, partWidth, tileWidth);
            // Fourth Row
            // Left Corner
            DrawTile(destOffset + partWidth, tileWidth * 3,
                offset, 104, partWidth, partWidth);
            // Middle
            DrawTile(destOffset + tileWidth, tileWidth * 3,
                offset, 112, tileWidth, partWidth);
            // Right Corner
            DrawTile(destOffset + tileWidth * 2, tileWidth * 3,
                offset + 8, 104, partWidth, partWidth);
        }

        private void DrawTile(int destX, int destY, int srcX, int srcY)
        {
            DrawTile(destX, destY, srcX, srcY,
                Constants.Tiles.Width, Constants.Tiles.Width);
        }

        private void DrawTile(int destX, int destY, int srcX, int srcY, int srcW, int srcH)
        {
            graphics.DrawImage(sourceBitmap, destX, destY,
                new Rectangle(srcX, srcY, srcW, srcH),
                GraphicsUnit.Pixel);
        }

        public string BaseSpecialFilename(string type)
        {
            return tileset.Identifier.ToString() + "_" + tileset.Name.Trim() +
                "_" + type + "_";
        }


    }
}
