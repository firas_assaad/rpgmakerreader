﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RpgMakerReader.Types
{
    public class Database
    {
        public Tileset[] tilesets { set; get; }
    }
}
