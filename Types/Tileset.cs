﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RpgMakerReader.Types
{
    public class Tileset
    {
        public enum SeaAnimationTypes
        {
            PingPong = 0,
            Forward = 1,
        }
        public enum SeaAnimationSpeeds
        {
            Slow = 0,
            Fast = 1,
        }
        public int Identifier { set; get; }
        public string Name { set; get; }
        public string Filename { set; get; }
        public int Width { set; get; }
        public int Height { set; get; }
        public Autotile[] Autotiles { set; get; }
        public uint[] TerrainTypes { set; get; }
        public uint[] LowerPassageInfo { set; get; }
        public uint[] UpperPassageInfo { set; get; }
        public SeaAnimationTypes SeaAnimationType { set; get; }
        public SeaAnimationSpeeds SeaAnimationSpeed { set; get; }

        public Tileset()
        {
            Name = "";
            Filename = "";
            SeaAnimationType = SeaAnimationTypes.PingPong;
            SeaAnimationSpeed = SeaAnimationSpeeds.Slow;
        }
    }
}
