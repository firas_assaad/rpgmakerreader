﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RpgMakerReader.Types
{
    public class Autotile
    {
        public int id { set; get; }
        public string filename { set; get; }
        public int parentId { set; get; }
    }
}
