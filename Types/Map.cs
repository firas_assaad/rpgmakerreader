﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;

namespace RpgMakerReader.Types
{
    [Serializable]
    public class Map
    {
        // Scroll types
        public enum ScrollTypes
        {
            None = 0,
            Vertical = 1,
            Horizontal = 2,
            VerticalAndHorizontal = 3,
        }
        public int TilesetID { set; get; }
        public int Width { set; get; }
        public int Height { set; get; }
        public ScrollTypes LoopType { set; get; }
        public bool HasPanorama { set; get; }
        public string PanoramaName { set; get; }
        public bool HasHorizontalScroll { set; get; }
        public bool HasVerticalScroll { set; get; }
        public bool HasAutoHorizontalScroll { set; get; }
        public int AutoHorizontalScrollSpeed { set; get; }
        public bool HasAutoVerticalScroll { set; get; }
        public int AutoVerticalScrollSpeed { set; get; }
        public uint[] LowerLayer { set; get; }
        public uint[] UpperLayer { set; get; }
        public object Events { set; get; }
        public int SaveTime { set; get; }

        public Map()
        {
            TilesetID = 1;
            Width = 20;
            Height = 15;
            LoopType = ScrollTypes.None;
        }

        public static void Save(Map map, string filename)
        {
            var xmlSerializer = new XmlSerializer(typeof(Map));
            xmlSerializer.Serialize(File.OpenWrite(filename), map);
        }

        public static Map Load(string filename)
        {
            var xmlSerializer = new XmlSerializer(typeof(Map));
            return (Map)xmlSerializer.Deserialize(File.OpenRead(filename));
        }

        
    }
}
