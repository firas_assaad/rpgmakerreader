﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RpgMakerReader.Types
{
    public class Tile
    {
        public enum TileTypes
        {
            Normal = 0,
            Animated = 1,
            Autotile = 2,
        }
        public TileTypes Type { set; get; }
        public uint Terrain { set; get; }
        public uint PassageInfo { set; get; }
    }
}
