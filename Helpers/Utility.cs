﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace RpgMakerReader.Helpers
{
    public class Utility
    {
        // https://github.com/take-cheeze/librpg2k
        private enum Ber
        {
            Bit = 0x7,      // 0111
            Sign = 0x80,    // 1000 0000
            Mask = 0x7f,    // 0111 1111
        }
        public static uint ReadBer(BinaryReader reader)
        {
            uint ret = 0;
            byte data;
            do
            {
                data = reader.ReadByte();
                ret = (ret << (int)Ber.Bit) | (data & (uint)Ber.Mask);
            }
            while (data > (int)Ber.Sign);
            return ret;
        }
        public static string ReadString(BinaryReader reader, uint length)
        {
            return new string(reader.ReadChars((int)length));
        }
        public static string ReadString(BinaryReader reader)
        {
            uint length = Utility.ReadBer(reader);
            return ReadString(reader, length);
        }
    }
}
