﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RpgMakerReader.Helpers
{
    class TileConverter
    {
        // Corner format:
        // [ABCD]
        // A = Upper left, B = Upper right
        // C = Lower left, D = Lower right
        private static uint[] autoTileMap =
        {
            0x66660, // 00 -> [E E E E] - [0000]
            0x66668, // 01 -> [E E E E] - [1000]
            0x66664, // 02 -> [E E E E] - [0100]
            0x6666C, // 03 -> [E E E E] - [1100]
            0x66661, // 04 -> [E E E E] - [0001]
            0x66669, // 05 -> [E E E E] - [1001]
            0x66665, // 06 -> [E E E E] - [0101]
            0x6666D, // 07 -> [E E E E] - [1101]
            0x66662, // 08 -> [E E E E] - [0010]
            0x6666A, // 09 -> [E E E E] - [1010]
            0x66666, // 10 -> [E E E E] - [0110]
            0x6666E, // 11 -> [E E E E] - [1110]
            0x66663, // 12 -> [E E E E] - [0011]
            0x6666B, // 13 -> [E E E E] - [1011]
            0x66667, // 14 -> [E E E E] - [0111]
            0x6666B, // 15 -> [E E E E] - [1111]
            0x22220, // 16 -> [D D D D] - [0000]
            0x22224, // 17 -> [D D D D] - [0100]
            0x22221, // 18 -> [D D D D] - [0001]
            0x22225, // 19 -> [D D D D] - [0101]
            0x55550, // 20 -> [B B B B] - [0000]
            0x55551, // 21 -> [B B B B] - [0001]
            0x55552, // 22 -> [B B B B] - [0010]
            0x55553, // 23 -> [B B B B] - [0011]
            0xAAAA0, // 24 -> [F F F F] - [0000]
            0xAAAA2, // 25 -> [F F F F] - [0010]
            0xAAAA8, // 26 -> [F F F F] - [1000]
            0xAAAAA, // 27 -> [F F F F] - [1010]
            0x77770, // 28 -> [H H H H] - [0000]
            0x77778, // 29 -> [H H H H] - [1000]
            0x77774, // 30 -> [H H H H] - [0100]
            0x7777C, // 31 -> [H H H H] - [1100]
            0x2A2A0, // 32 -> [D F D F] - [0000]
            0x55770, // 33 -> [B B H H] - [0000]
            0x11110, // 34 -> [A A A A] - [0000]
            0x11111, // 35 -> [A A A A] - [0001]
            0x99990, // 36 -> [C C C C] - [0000]
            0x99992, // 37 -> [C C C C] - [0010]
            0xBBBB0, // 38 -> [I I I I] - [0000]
            0xBBBB8, // 39 -> [I I I I] - [1000]
            0x33330, // 40 -> [G G G G] - [0000]
            0x33334, // 41 -> [G G G G] - [0100]
            0x19190, // 42 -> [A C A C] - [0000]
            0x11330, // 43 -> [A A G G] - [0000]
            0x3B3B0, // 44 -> [G I G I] - [0000]
            0x99BB0, // 45 -> [C C I I] - [0000]
            0x193B0, // 46 -> [A C G I] - [0000]
        };

        private static ushort[] tileStartPos =
        {
            0x0FA0,
            0x0FD2,
            0x1004,
            0x1036,
            0x1068,
            0x109A,
            0x10CC,
            0x10FE,
            0x1130,
            0x1162,
            0x1194,
            0x11C6,
            0x11F8,
        };

        public static uint ConvertTile(ushort rm2kTile)
        {
            uint position = 0;
            int minimum = tileStartPos[0];
            int maximum =
                tileStartPos[tileStartPos.Length - 1] + tileStartPos[1] - minimum;
            if (rm2kTile >= minimum && rm2kTile < maximum)
            {
                for (; position < tileStartPos.Length; position++)
                {
                    if (tileStartPos[position] > rm2kTile)
                    {
                        position--;
                        break;
                    }
                }

                int tilePos = rm2kTile - tileStartPos[position];
                return (position << 20) | autoTileMap[tilePos];
            }
            else
            {
                return autoTileMap[0];
            }
        }
    }
}
