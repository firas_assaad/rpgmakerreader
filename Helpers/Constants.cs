﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RpgMakerReader.Helpers
{
    public static class Constants
    {
        // File Types
        public static class Files
        {
            public static string Database { get { return "RPG_RT.ldb"; } }
            public static string MapPattern { get { return "Map*.lmu"; } }
            public static string MapExtension { get { return ".mpf"; } }
            public static string ImageFormat { get { return ".png"; } }
            public static string Autotile { get { return "autotile"; } }
            public static string Animated { get { return "animated"; } }
        }
        // Directories
        public static class Directories
        {
            public static string DestinationPrefix { get { return "Converted_"; } }
            public static string DestMaps { get { return "Maps\\"; } }
            public static string SrcTilesets { get { return "ChipSet\\"; } }
            public static string DestTilesets { get { return "Tilesets\\"; } }
            public static string DestSpecialTiles { get { return "Special\\"; } }
            public static string DestImages { get { return "Images\\"; } }
            public static string RTP2000 { get { return "C:\\Program Files (x86)\\ASCII\\RPG2000\\RTP\\"; } }
        }
        // Other constants
        public static class Tiles
        {
            public static int Width { get { return 16; } }
            public static int NormalTiles { get { return 288; } }
            public static int AnimatedTiles { get { return 3; } }
            public static int AnimationFrames { get { return 3; } }
            public static int AnimatedWidth { get { return AnimationFrames * Width; } }
            public static int AutoTiles { get { return 15; } }
            public static int AutoTilesWidth { get { return 3 * Width; } }
            public static int AutoTilesHeight { get { return 4 * Width; } }
            public static int AnimatedAutoTiles { get { return 3; } }
            public static int AnimatedAutoTilesWidth { get { return AnimationFrames * AutoTilesWidth; } }
            public static int DestTilesPerRow { get { return 6; } }
        }
    }
}
